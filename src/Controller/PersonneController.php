<?php

namespace App\Controller;

use App\Entity\Personne;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use App\Repository\PersonneRepository;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\JsonResponse;

class PersonneController extends AbstractController
{
    private $personneRepository;
    public function __construct(
        PersonneRepository $personneRepository
    ) {
        $this->personneRepository = $personneRepository;
    }

    /**
     * @Route("/personne", name="personne",  methods={"POST"})
     */
    public function createPersonne(
        Request $request,
        EntityManagerInterface $em
    ): Response {
        $parameters = json_decode($request->getContent(), true);

        if (!isset($parameters['nom'])) {
            return new JsonResponse(["error" => "Le nom n'existe pas"], Response::HTTP_BAD_REQUEST);
        }
        if (!isset($parameters['prenom'])) {
            return new JsonResponse(["error" => "Le prenom n'existe pas"], Response::HTTP_BAD_REQUEST);
        }
        if (!isset($parameters['date-naissance'])) {
            return new JsonResponse(["error" => "La date de naissance n'existe pas"], Response::HTTP_BAD_REQUEST);
        }

        $personne = new Personne();


        $date_naissance = new \DateTime($parameters['date-naissance']);
        $age = $personne->calculAge($date_naissance);

        if (!$personne->validateAge($age)) {
            return new JsonResponse(["error" => "L'age n'est pas correcte"], Response::HTTP_BAD_REQUEST);
        }

        $personne->setNom($parameters['nom']);
        $personne->setPrenom($parameters['prenom']);
        $personne->setDateOfBirth($date_naissance);

        $em->persist($personne);
        $em->flush();

        return new JsonResponse(["reponse" => "success"], Response::HTTP_OK);
    }

    /**
     * @Route("/personnes", name="personnes",  methods={"GET"})
     */
    public function getPersonne(): Response
    {
        $personnes = $this->personneRepository->findAll();

        return new JsonResponse(
            array_map(function (Personne $personne) {
                return $personne->toArray();
            }, $personnes),
            Response::HTTP_OK
        );
    }
}
