Endpoints: POST / GET

"Create person"
Method: POST
url : {base_url}/personne
body:
{
	"nom" : "My_lastname",
	"prenom" : "My_name",
	"date-naissance" : "2000/01/01"
}

"Get all persons"
Method: GET
url : {base_url}/personnes
